package pcd.lab05.monitors.barrier;

/*
 * Barrier - to be implemented
 */
public class FakeBarrier implements Barrier {

	private int nParticipants;
	//private Lock mutex;
	//private Condition cond;
	
	public FakeBarrier(int nParticipants) {
		this.nParticipants = nParticipants;
		//mutex = new ReentrantLock();
		//cond = mutex.newCondition();
	}
	
	@Override
	public synchronized void hitAndWaitAll() throws InterruptedException {
		while(nParticipants != 1) {
			nParticipants --;
			wait();
		}
		notifyAll();
		/*
		 * mutex.lock();
		 * nPartecipants --;
		 * if(nPartecipants > 0)
		 *  cond.await();
		 * else 
		 * 	cond.signalAll();
		 * mutex.unlock();
		 */
	}
	
}
