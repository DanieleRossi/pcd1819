package pcd.lab05.monitors.cyclic_barrier.sol;

public interface Barrier {

	void hitAndWaitAll() throws InterruptedException;

}
