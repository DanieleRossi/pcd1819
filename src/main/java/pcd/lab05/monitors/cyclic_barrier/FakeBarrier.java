package pcd.lab05.monitors.cyclic_barrier;

/*
 * Barrier - to be implemented
 */
public class FakeBarrier implements Barrier {

	private int nParticipants;
	private int nPartecipantsWaiting;
	private boolean isAwakeSignal = false;
	
	public FakeBarrier(int nParticipants) {
		this.nParticipants = nParticipants;
		this.nPartecipantsWaiting = 0;
	}
	
	@Override
	public synchronized void hitAndWaitAll() throws InterruptedException {
		while (nPartecipantsWaiting < nParticipants-1 && !isAwakeSignal) {
			nPartecipantsWaiting++;
			wait();
			nPartecipantsWaiting--;
		}
		if(nParticipants-1 == nPartecipantsWaiting) {
			isAwakeSignal = true;
			notifyAll();
		}
		if(nPartecipantsWaiting == 0) {
			isAwakeSignal = false;
		}

	}

	
}
