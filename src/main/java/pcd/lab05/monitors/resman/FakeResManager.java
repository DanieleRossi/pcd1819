package pcd.lab05.monitors.resman;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Semaphore;

public class FakeResManager implements ResManager {
	List<Integer> resources;

	public FakeResManager(int nResourcesAvailable) {
		resources = new ArrayList<Integer>(nResourcesAvailable);
		for(int i=0; i<nResourcesAvailable; i++) {
			resources.add(i);
		}
	}
	
	@Override
	public synchronized int get() throws InterruptedException {
		while(resources.size() == 0) {
			wait();
		}
		int res = resources.get(0);
		resources.remove(0);
		return res;
	}

	@Override
	public synchronized void release(int id) {
		resources.add(id);
		notifyAll();
	}

}
