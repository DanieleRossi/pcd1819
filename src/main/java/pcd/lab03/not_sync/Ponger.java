package pcd.lab03.not_sync;

import java.util.concurrent.Semaphore;

public class Ponger extends Thread {

	private Counter counter;
	
	public Ponger(Counter c) {
		counter = c;
	}	
	
	public void run() {
		while (true) {
			try {
				counter.inc();
				System.out.println("pong!"+counter.getValue());
				Thread.sleep(1000);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}
}