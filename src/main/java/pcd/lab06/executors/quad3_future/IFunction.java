package pcd.lab06.executors.quad3_future;

public interface IFunction {

	public double eval(double val);
	
}
