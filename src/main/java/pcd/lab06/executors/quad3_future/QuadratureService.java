package pcd.lab06.executors.quad3_future;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

public class QuadratureService  {

	private int numTasks;
	private ExecutorService executor;
	private List<Future<Double>> futures;
	
	public QuadratureService (int numTasks, int poolSize){		
		this.numTasks = numTasks;
		executor = Executors.newFixedThreadPool(poolSize);
		futures = new ArrayList<>(numTasks);
	}
	
	public double compute(IFunction mf, double a, double b) throws InterruptedException { 
	
		double x0 = a;
		double step = (b-a)/numTasks;		
		for (int i=0; i<numTasks; i++) {
			try {
				futures.add(executor.submit(new ComputeAreaTask(x0, x0 + step, mf)));
				log("submitted task "+x0+" "+(x0+step));
				x0 += step;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return futures.stream().map(f -> {
			try {
				return f.get();
			} catch (InterruptedException | ExecutionException e) {
				System.err.println("An error occurred in one of the tasks.\n Error details: "+e.getMessage());
				System.exit(1);
				return null;
			}
		}).reduce(Double::sum).get();
	}
	
	
	private void log(String msg){
		System.out.println("[SERVICE] "+msg);
	}
}
