package pcd.lab04.mandel_wrong;

public interface InputListener {

	void started(Complex c0, double diam);
	
	void stopped();
}
