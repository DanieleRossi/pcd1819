package pcd.lab04.mandel_ok_noconcur;

public interface InputListener {

	void started(Complex c0, double diam);
	
	void stopped();
}
